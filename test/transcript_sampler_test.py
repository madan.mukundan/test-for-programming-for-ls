import pytest
from code.transcript_sampler import TranscriptSampler

ts = TranscriptSampler()

@pytest.mark.parametrize(
    "test_data, expected_data",
    [("./test/PLSe1geneData.test",
    {'T1': 1, 'T2': 2, 'T3': 10, 'T4': 25, 'T5': 8, 'T6': 2, 'T7': 5, 
    'T8': 30, 'T9': 10, 'T10': 8})])

def test_read_avg_expression(test_data, expected_data):
    assert type(ts.read_avg_expression('./test/PLSe1geneData.test'))==dict
    assert ts.read_avg_expression(test_data)==expected_data

test_nos_to_sample = [5, 50, 500, 5000, 50000]
@pytest.mark.parametrize(
    "test_number",
    test_nos_to_sample
)

def test_sample_transcripts(test_number):
    test_data=ts.read_avg_expression('./test/PLSe1geneData.test')
    assert type(ts.sample_transcripts(test_data,test_number))==dict
    test_sample = ts.sample_transcripts(test_data,test_number)
    assert sum(test_sample.values()) == test_number
