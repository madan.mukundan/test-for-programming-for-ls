"""Module for TranscriptSampler class for simple gene expression data.

Leave one blank line.  The rest of this docstring should contain an
overall description of the module or program.  Optionally, it may also
contain a brief description of exported classes and functions and/or usage
examples.

  Typical usage example:

  ts = TranscriptSampler()
  average_expressions = ts.read_avg_expression(file)
  transcript_sample = ts.sample_transcript(avg_exp, no_to_sample)
  ts.write_sample(file, transcript_sample)
"""


import os
import pathlib
import numpy as np
from collections import Counter


class TranscriptSampler ():
    """A class to store and sample transcript data.

    This class generates a transcript sampler that can read in gene expression
    information from a file and generates a sample a new gene expression
    measurement using the relative bundance provided in the file.
    """
    def read_avg_expression(self, file) -> dict:
        """Reads a gene expression file formatted 'gene_id no_copies'.

        Reads in gene expression file with the format 'gene_id no_copies'.
        Returns a dictionary of the values of each row.

        Args:
            file (string): name of the gene expression data file.

        Returns:
            dictionary: expression data represented in file
        """
        # initialize empty dictionary
        avgExpression = {}
        # read file in line by line
        file = os.path.abspath(os.path.join(pathlib.Path.cwd(), file))
        with open(file, 'r') as myfile:
            for currentLine in myfile:
                # process and assign gene_id no_copies into dictionary
                lineInfo = currentLine.strip().split()
                gene_id = lineInfo[0]
                nr_copies = int(lineInfo[1])
                avgExpression[gene_id] = nr_copies
        return avgExpression

    def sample_transcripts(self, avgs, number) -> dict:
        """Simulates a RNAseq sampling experiment from given relative abundances.

        Samples a defined number of reads with respect to relative abundance
        and returns a dictionary with the sampled reads.

        Args:
            avgs (dictionary): dictionary containing gene expression counts.
            number (int): number of the transcripts to be sampled.

        Returns:
            dictionary: dictionary containing the sampled transcript counts.
        """
        # calculate relative abundance of genes based on transcript data
        totalReads = sum(list(avgs.values()))
        freq = [a/totalReads for a in avgs.values()]
        # randomly sample no_to_sample genes based on relative abundance
        rawSamples = np.random.choice(list(avgs.keys()), size=number, p=freq)
        samples = dict(Counter(rawSamples))
        return samples

    def write_sample(self, file, sample) -> os.write:
        """Writes a simulated random sample of genes based on relative abundance.

        Writes the transcript count data in a dictionary as gene_id: nr_copies.

        Args:
            file (string): name of write target file.
            sample(dictionary): dictionary containing transcript sample data.
        """
        with open(file, 'w') as myfile:
            # user warning - current write_target file will be overwritten
            overwriteChoice = \
                input("\nI will overwrite current file, ok? y/n: ")
            # writes the data directly to the file as an unformatted dictionary
            if overwriteChoice == 'y':
                print(sample, file=myfile)
                print('\nWrite success!\n')
            # write abort message
            else:
                print('\nWrite aborted\n')
        return myfile
