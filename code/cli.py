"""CLI for TranscriptSampler class."""

import argparse
import sys
from transcript_sampler import TranscriptSampler


def main():
    r"""CLI for TranscriptSampler, argins data_file, no_to_sample, write_target.

    This cli is used to access the functionality of the TranscriptSampler
    class, taking argsin data_file, which contains a list of transcripts
    and incidences formatted 'gene_id no_copies'; no_to_sample, an int
    indicating the # of samples that the sample_transcripts function should
    return; and write_target_file, a file which will be written over by the
    write_sample function.

    Typical example usage:

    cli.py '..\test\PLSe1geneData.test' 50 '..\test\PLSe1writeTarget.test'
    """
    parser = argparse.ArgumentParser(
        description='number of transcripts to sample')
    parser.add_argument('data_file', action='store',
                        type=str, help='gene expression data file')
    parser.add_argument('no_to_sample', action='store',
                        type=int, help='number of samples to be simulated')
    parser.add_argument('write_target', action='store',
                        type=str, help='write simulation sample data to file')
    argsin = parser.parse_args()

    try:
        ts = TranscriptSampler()

        avg_expression = ts.read_avg_expression(argsin.data_file)

        print('\n Input file: ' +
              argsin.data_file + ', containing these data:')
        print(avg_expression)

        print('\nI will sample ' + str(argsin.no_to_sample))
        ts_sample = ts.sample_transcripts(avg_expression, argsin.no_to_sample)
        print(ts_sample)
        ts.write_sample(argsin.write_target, ts_sample)

    except Exception:
        print('\nInvalid input, please check file names'
              ' and that sample number is positive (int) \n')
        sys.exit()


if __name__ == "__main__":
    main()
