from setuptools import setup, find_packages

setup(
    name='Transcript_Sampler_PLS',
    url='https://gitlab.com/madan.mukundan/test-for-programming-for-ls',
    author='Madan Mukundan',
    author_email='madan.mukundan@iob.ch',
    description='Example Package for PLS Class, takes in int to return a random sample of transcripts from input file',
    license='MIT',
    version='1.0.0',
    packages=find_packages(),
    install_requires=[
        'numpy',
        'argparse'
    ],
    entry_points={'console_scripts': ['generate_sample=Transcript_Sampler_PLS.cli:main']}
)